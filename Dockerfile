FROM jupyter/base-notebook:latest
USER root
WORKDIR /app
COPY . /app
RUN pip install --requirement /app/requirements.txt
CMD ["start-notebook.sh"]