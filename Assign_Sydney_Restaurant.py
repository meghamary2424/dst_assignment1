#!/usr/bin/env python
# coding: utf-8

# In[1]:


import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import geopandas as gp
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression, SGDRegressor
from sklearn.metrics import mean_squared_error
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import confusion_matrix, classification_report,ConfusionMatrixDisplay
from sklearn.tree import DecisionTreeClassifier
from sklearn.svm import SVC


# In[2]:


# Load the dataset 
data = pd.read_csv("data\zomato_df_final_data.csv")
data.head()


# ## PART A

# In[3]:


# How many unique cuisines are served by Sydney restaurants?

unique_cuisine = set()  # empty set to store unique cuisines for each restaurant

for index in range(len(data)):
    row = data.iloc[index]  
    cuisine_new_list = row['cuisine'].strip('[]').replace("'", "").split(', ') #extract the list of cuisines
    unique_cuisine.update(cuisine_new_list)     # update each cuisine to the set

# Get the count of unique cuisines
no_unique_cuisine = len(unique_cuisine)

print(f" Total No: of unique cuisines served by Sydney restaurants: {no_unique_cuisine}")

# a bar plot for the number of unique cuisines
plt.barh(["Unique Cuisine"], [no_unique_cuisine], color='green')
plt.title('Total no: of Unique Cuisines Served by Sydney Restaurants')
plt.xlabel('No: of Unique Cuisines')
plt.show()


# In[4]:


# Which suburbs (top-3) have the highest number of restaurants?
top3_suburbs = data['subzone'].value_counts().head(3)
print("Most popular suburbs")
print(top3_suburbs)

# Plot the top suburbs
plt.figure(figsize=(8, 6))
top3_suburbs.plot(kind='bar', color='grey')
plt.title('Most popular suburbs for the restuarant')
plt.xlabel('Suburb')
plt.ylabel('No: of Restaurants')
plt.show()


# In[5]:


# comparison between 'cost' and 'rating_text using '
sns.histplot(data, x='cost', hue='rating_text', multiple='stack', bins=20, palette='coolwarm')
plt.title("Relationship between Cost and Rating")
plt.xlabel("Cost")
plt.ylabel("Count")
plt.show()


# From the above graph ,it is very clear that the restaurants with excellent rating are highly expensive while poor rating are rarely expensive.

# In[6]:


# Perform exploratory analysis for the variables of the data.
#Cost
plt.figure(figsize=(12, 4))
plt.subplot(1, 3, 1)
sns.histplot(data['cost'], bins=30)
plt.title('Cost Distribution')

#Rating_number
plt.subplot(1, 3, 2)
sns.histplot(data['rating_number'], bins=20)
plt.title('Rating Distribution')

#Type
plt.subplot(1, 3, 3)
sns.countplot(data['type'])
plt.title('Restaurant Types')
plt.xticks(rotation=45)
plt.tight_layout()
plt.show()


# From the cost distribution graph, it can be seen that histogram is right skewed that is there are few restauarants with high cost but the majority of restaurants have more affordable pricing ,while in rating distribution, there are relatively few restaurants with extremely low or high ratings, indicating that most restaurants fall within the average rating range. The type distribution shows that most common restaurant type in the dataset is Casual Dining followed by other types like Quick Bites and cafe.

# In[30]:


#Create a map illustrating cuisine density, where each suburb is visually
#represented with colors indicating the number of restaurants offering a specific cuisine."

# Function to plot cuisine density map
def show_cuisinemap(cuisine):
    geo_data =gp.read_file("data/sydney.geojson")
    # Filter restaurants that serve the specified cuisine
    geo = gp.GeoDataFrame(data, geometry=gp.points_from_xy(data.lng, data.lat))
    filtered_gdf = geo[geo['cuisine'].str.contains(cuisine, case=False, na=False)]
  
    joined_gdf = gp.sjoin(geo_data, filtered_gdf, how='left', predicate='contains')   # spatial join

    joined_gdf['count'] = joined_gdf.groupby('subzone')['subzone'].transform('count') # Count the number of restaurants in each suburb

    # Plot
    fig, ax = plt.subplots(1, figsize=(12, 8))
    joined_gdf.boundary.plot(ax=ax, linewidth=1)
    joined_gdf.plot(column='count', ax=ax, legend=True,
                    legend_kwds={'label':  cuisine},
                    cmap='viridis', linewidth=0.6, edgecolor='0.5')

    plt.title(f"{cuisine} Cuisine Density Graph")
    plt.show()

show_cuisinemap('Indian')

show_cuisinemap('Chinese')


# In[8]:


data.head()


# # Part B -Predictive Modelling

# ### 1.Feature engineering

# ##### Data cleaning and preprocessing

# In[9]:


#Check for null values in the dataset
null_values = data.isnull().sum()
print(null_values)

#Drop rows with NA in rating_number
data.dropna(subset=['rating_number'], inplace=True)

#Handle column cuisine which is in lists by spliting comma-separated values
data['cuisine'] = data['cuisine'].str.split(', ')
data = data.explode('cuisine') #Explode the lists into separate rows


# In[10]:


#Cheching for categorical variables
categorical_variable = [var for var in data.columns if data[var].dtype=='O']
data[categorical_variable].head()
print("The categorical variables are: ", categorical_variable)

#checking for numerical variables
numerical_variable = [var for var in data.columns if data[var].dtype != 'O']
print('The numerical variables are :', numerical_variable)


# In[11]:


#Handle missing values in categorical variables
#Replace missing values in 'type' column with 'unknown'
data['type'].fillna('unknown', inplace=True)

# Handle missing values in numerical variables
# Replace missing values in 'cost' column with the mean cost
data['cost'].fillna(data['cost'].mean(), inplace=True)


# ##### Feature Encoding

# In[12]:


## Encode categorical features
label_encoder = LabelEncoder()
data['type_encode'] = label_encoder.fit_transform(data['type'])
data['subzone_encode'] = label_encoder.fit_transform(data['subzone'])
data['rating_encode'] = label_encoder.fit_transform(data['rating_text'])
data['cuisine_encode'] = label_encoder.fit_transform(data['cuisine'])
data['title_encode'] = label_encoder.fit_transform(data['title'])


# In[13]:


data.head()


# ### Regression

# In[14]:


# Select features and target variable
X = data[['type_encode', 'subzone_encode','cost','cuisine_encode','rating_encode']]
y = data['rating_number']


# In[15]:


# Split the data into train (80%) and test (20%) sets
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)


# ##### Model 1

# In[16]:


# Linear Regression Model 1
model1_regression = LinearRegression()
model1_regression.fit(X_train, y_train)


# In[17]:


y_pred = model1_regression.predict(X_test)

# Calculate Mean Squared Error (MSE)
mse = mean_squared_error(y_test, y_pred)
print(f"Mean Squared Error (MSE) for Model 1: {mse}")


# ##### Model 2

# In[18]:


# Linear Regression Model 2 with Gradient Descent
model2_regression = SGDRegressor(max_iter=1000, tol=1e-3, random_state=0)
model2_regression.fit(X_train, y_train)

y_pred_2 = model2_regression.predict(X_test)

# Calculate Mean Squared Error (MSE) for Model 2
mse_2 = mean_squared_error(y_test, y_pred_2)
print("Mean Squared Error (MSE) for Model 2:", mse_2)


# ### Classification

# In[19]:


# Simplify the problem into binary classifications:
# Class 1: 'Poor' and 'Average' records
# Class 2: 'Good', 'Very Good', and 'Excellent' records
data['class'] = data['rating_text'].apply(lambda x: 'Class 1' if x in ['Poor', 'Average'] else 'Class 2')


# In[20]:


#Select features and target variable
X = data[['type_encode', 'subzone_encode','cost','cuisine_encode','rating_encode','title_encode']]
y = data['class']


# In[21]:


# Split the data into train (80%) and test (20%) sets
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)

# Create and fit the Logistic Regression model for binary classification
model3_classification = LogisticRegression(random_state=0)
model3_classification.fit(X_train, y_train)


# In[22]:


# Make predictions on the test set
y_pred3 = model3_classification.predict(X_test)

# Calculate the confusion matrix
confusion_matrix = confusion_matrix(y_test, y_pred3)

# Print the confusion matrix
print(confusion_matrix)

# Generate and print a classification report
performance_report = classification_report(y_test, y_pred3)
print("Performance Report:")
print(performance_report)

#Display Confusion matrix
disp = ConfusionMatrixDisplay(confusion_matrix, display_labels=['Class 1', 'Class 2'])
disp.plot(cmap='Accent', values_format='d')


# The model exhibits a low number of false positives and false negatives, which shows that it performs well in accurately distinguishing between Class 1 and Class 2.From the performance report, it is very clear that the model achieves a high accuracy ,that is about 97% which indicates that it correctly classifies into their respective classes (Class 1 and Class 2).
# Overall, the model produces high accuracy,precision and recall for both classes.

# ### Bonus points

# ##### SVM Model

# In[23]:


model_svm = SVC(random_state=0)
model_svm.fit(X_train, y_train)
y_pred_svm = model_svm.predict(X_test)
print(classification_report(y_test, y_pred_svm))


# ##### Decision Tree

# In[24]:


model_dt = DecisionTreeClassifier(random_state=0)
model_dt.fit(X_train, y_train)
y_pred_dt = model_dt.predict(X_test)
print(classification_report(y_test, y_pred_dt))


# In[ ]:




