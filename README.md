# Sydney Restaurant Analysis
This project mainly focuses on the analysis of restaurant data in Sydney, which helps in the prediction of the restaurant’s success using different machine learning algorithms. This mainly involves regression and classifcation tasks to extract insights about restaurant ratings, pricing, and cuisine offerings and so on.
This project is divided into three parts:
- Importing and understanding of data which performs exploration of data
- Predictive modelling which involves building predictive models using machine learning techniques. Two types of model are used:
    - Linear regression
    - Logistic regression
- Deployment

# Data Sources
The dataset used in this project can be accessed from Sydney Restaurant Dataset which is in data folder. It contains information about various restaurants in Sydney, including their location, cost, cuisine, and ratings and so on.

# Code
The code file is placed in Assign_Sydney_Restaurant python file.
 
# Tableau Visualization
We have created a Tableau visualization to provide interactive insights into the restaurant data. You can find the Tableau file, Tableau_Assign.twb, in the repository.

# Running the Code
- Build the docker image:
    docker build -t name
- Bull the docker image :docker pull meghamary/dst_assign1:latest
- Run a Docker Container:
    docker run -p name

# Dependencies
The list of dependencies that have been used in the code can be found in requirements text file.

# Expected Output
The code will generate predictive models, complete with performance metrics. You'll be able to understand how well these models predict restaurant ratings and classify them based on various factors.Additionally,the code also enables you to interact with Tableau visualizations, which offers a dynamic and visual representation of restaurant data.

# Conclusion
The project provides a comprehensive analysis of restaurant data of Sydney, by including predictive models and visualizations. 

# Contact
Feel free to reach if you have any questions
Megha Mary Varghese (u3238990@uni.canberra.edu.au)
